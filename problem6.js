
function intrestedCars(inventory = [], intrestedcars = []) {
    if (Array.isArray(inventory) || Array.isArray(intrestedCars)) {
        intrestedcars = intrestedcars.join(' ')
        let filteredCars = []
        for (i = 0; i < inventory.length; i++) {
            let makedata = inventory[i].car_make
            if (intrestedcars.includes(makedata)) {
                filteredCars.push(inventory[i])
            }
        }
        return filteredCars
    }
    else {
        return []
    }
}
module.exports = intrestedCars
