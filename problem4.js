
function getYears(inventory = []) {
    if (Array.isArray(inventory)) {
        let Years = []

        for (i = 0; i < inventory.length; i++) {
            Years.push(inventory[i].car_year)
        }
        return Years
    }
    else {
        return []
    }
}
module.exports = getYears