function getLastInventory(inventory = []) {
    if (Array.isArray(inventory)) {
        try {
            let lastInventory = inventory[inventory.length - 1]
            let data = `last car is a ${lastInventory.car_make}  ${lastInventory.car_model}`
            return data
        }
        catch {
            return []
        }
    }
    else {
        return []
    }
}
module.exports = getLastInventory