function getSortedByModel(inventory = []) {
    if (Array.isArray(inventory)) {
        let sortedInventory = inventory.sort((a, b) => a.car_model > b.car_model ? 1 : -1)
        return sortedInventory
    }
    else {
        return []
    }
}
module.exports = getSortedByModel