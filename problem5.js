
function olderThan2000(inventory = []) {
    if (Array.isArray(inventory)) {
        let counter = 0
        for (i = 0; i < inventory.length; i++) {
            if (inventory[i].car_year < 2000) {
                counter = counter + 1
            }
        }

        return counter

    }
    else {
        return []
    }
}
module.exports = olderThan2000